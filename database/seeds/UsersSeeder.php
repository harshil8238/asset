<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'phonenumber' => '1111111111',
            'address' => 'address',
            'countryId' => '000',
            'stateId' => '000',
            'cityId' => '000',
            'pincode' => '000',
        ]);
    }
}
