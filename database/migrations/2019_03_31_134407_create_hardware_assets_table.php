<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHardwareAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hardware_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hardwarecategoryId');
            $table->string('model');
            $table->string('srno');
            $table->integer('vendorId');
            $table->integer('price');
            $table->timestamp('purchasedate');
            $table->integer('warantee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hardware_assets');
    }
}
