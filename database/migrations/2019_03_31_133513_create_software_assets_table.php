<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('software_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catagoryId');
            $table->integer('softwareId');
            $table->string('productkey');
            $table->string('srno');
            $table->integer('vendorId');
            $table->integer('price');
            $table->timestamp('purchasedate');
            $table->integer('warantee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_assets');
    }
}
