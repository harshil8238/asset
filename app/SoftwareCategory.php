<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoftwareCategory extends Model
{
    protected $table = 'software_categories';
}
