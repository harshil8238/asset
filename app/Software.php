<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Software extends Model
{
    protected $table = 'softwares';

    public function software_category()
    {
        return $this->belongsTo('App\SoftwareCategory');
    }
}
