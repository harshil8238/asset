<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HardwareAsset extends Model
{
    protected $table = 'hardware_assets';
}
