<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SoftwareRequest;
use App\SoftwareCategory;
use App\Software;
use App\Vendor;
use App\SoftwareAsset;
use Illuminate\Support\Facades\Input;

class SoftwareController extends Controller
{
    public function index(Request $request)
    {
        $softwareAsset = SoftwareAsset::select('software_assets.id','software_assets.srno','softwares.software','software_categories.software_category','vendors.firstname','vendors.lastname','software_assets.price','software_assets.purchasedate','software_assets.warantee')
        ->leftjoin('softwares', 'software_assets.softwareId', '=', 'softwares.id')
        ->leftjoin('software_categories', 'software_assets.catagoryId', '=', 'software_categories.id')
        ->leftjoin('vendors', 'software_assets.vendorId', '=', 'vendors.id')
        ->get();
  
        return view('software.software', ['softwareAsset' => $softwareAsset]);
    }

    public function create(Request $request)
    {
        $softwareCategory = SoftwareCategory::orderby('id', 'DESC')->get()->keyby('id');
        $vendor = Vendor::all()->keyBy('id');
        return view('software.addSoftware', ['softwareCategory' => $softwareCategory, 'vendor' => $vendor]);
    }

    public function store(SoftwareRequest $request)
    {
        $softwareAsset = new SoftwareAsset;
        $softwareAsset->catagoryId = $request->catagoryId;
        $softwareAsset->softwareId = $request->softwareId;
        $softwareAsset->productkey = $request->productkey;
        $softwareAsset->srno = $request->srno;
        $softwareAsset->vendorId = $request->vendorId;
        $softwareAsset->price = $request->price;
        $softwareAsset->purchasedate = $request->purchasedate;
        $softwareAsset->warantee = $request->warantee;
        $softwareAsset->save();
     
        return redirect('software/create')->with('success', 'Software Add Successfully'); 
    }

    public function destroy(Request $request)
    {
        $softwareAsset = SoftwareAsset::where('id' ,'=', $request->deleteId)->first();
        $softwareAsset->delete();
        return redirect('hardware');
    }

    public function getSoftwareName(Request $request)
    {
        $softwareSubcategory = Software::where('software_category_id','=',$request->categoryID)
        ->orderby('id', 'DESC')->get()->keyby('id');
        return response()->json($softwareSubcategory);
    }
    public function getSearchSoftwareAsset(Request $request)
    {
        $q = Input::get ( 'q' );
        $softwareAsset = SoftwareAsset::select('software_assets.id','software_assets.srno','softwares.software','software_categories.software_category','vendors.firstname','vendors.lastname','software_assets.price','software_assets.purchasedate','software_assets.warantee')
        ->join('softwares', 'software_assets.softwareId', '=', 'softwares.id')
        ->join('software_categories', 'software_assets.catagoryId', '=', 'software_categories.id')
        ->join('vendors', 'software_assets.vendorId', '=', 'vendors.id')
        ->where('srno','LIKE','%'.$q.'%')->orWhere('price','LIKE','%'.$q.'%')->orWhere('software','LIKE','%'.$q.'%')->get();
        if(count($softwareAsset) > 0){
            return view('software.software', ['softwareAsset' => $softwareAsset]);
        }
        else {
            return view ('software.software', ['softwareAsset' => $softwareAsset]);
        }
       
    }
    

}
