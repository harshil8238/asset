<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SoftwareCategory;
use App\Software;
use App\Hardware;

class AssetController extends Controller
{
    public function createSoftwareCategory(Request $request)
    {
        return view('asset.createSoftwareCategory');
    }

    public function softwareCategory(Request $request)
    {
        $softwareCategory = SoftwareCategory::orderBy('id', 'DESC')->get();
        return view('asset.softwareCategory', ['softwareCategory' => $softwareCategory]);
    }

    public function storeSoftwareCategory(Request $request)
    {
        $validatedData = $request->validate([
            'software_category' => 'required',
        ]);
        
        $softwareCategory = new SoftwareCategory;
        $softwareCategory->software_category = $request->software_category;
        $softwareCategory->save();
        return redirect('asset/createSoftwareCategory')->with('success', 'Category Add Successfully');
    }

    public function destroySoftwareCategory(Request $request)
    {
        $softwareCategory = SoftwareCategory::where('id' ,'=', $request->deleteId)->first();
        
        $softwareCategory->delete();
        return redirect('asset/softwareCategory');
    }

    public function createSoftware(Request $request)
    {
        $softwareCategory = SoftwareCategory::all()->keyBy('id');
        return view('asset.createSoftware', ['softwareCategory' => $softwareCategory]);
    }

    public function software(Request $request)
    {
        $software = Software::orderBy('id', 'DESC')->get();
        return view('asset.software', ['software' => $software]);
    }

    public function storeSoftware(Request $request)
    {
        $validatedData = $request->validate([
            'software_category_id' => 'required',
            'software' => 'required',
            'photo' => 'required',
        ]);

        $software = new Software;
        $software->software_category_id = $request->software_category_id;
        $software->software = $request->software;
        $software->photo = $request->file('photo')->store('img', 'public');
        $software->save();
        return redirect('asset/createSoftware')->with('success', 'Software Add Successfully');

    }
    
    public function createHardware(Request $request)
    {
        return view('asset.createHardware');
    }
 
    public function hardware(Request $request)
    {
        $hardware = Hardware::orderBy('id', 'DESC')->get();
        
        return view('asset.hardware', ['hardware' => $hardware]);
    }

    public function storeHardware(Request $request)
    {
        $validatedData = $request->validate([
            'hardware' => 'required',
            'photo' => 'required',
        ]);

        $hardware = new Hardware;
        $hardware->hardware = $request->hardware;
        $hardware->photo = $request->file('photo')->store('img', 'public');
        $hardware->save();
        return redirect('asset/createHardware')->with('success', 'Hardware Add Successfully');
    }
    
    public function deleteHardware(Request $request)
    {
    }
}
