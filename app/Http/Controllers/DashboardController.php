<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Vendor;
use App\HardwareAsset;
use App\SoftwareAsset;


class DashboardController extends Controller
{
    public function getInfo() {
        $user = User::All();
        $vendor = Vendor::All();
        $hardwareAsset = HardwareAsset::select('hardware_assets.id','hardwares.hardware','hardware_assets.model','hardware_assets.srno','vendors.firstname','vendors.lastname','hardware_assets.price','hardware_assets.purchasedate','hardware_assets.warantee')
        ->join('hardwares', 'hardware_assets.hardwarecategoryId', '=', 'hardwares.id')
        ->join('vendors', 'hardware_assets.vendorId', '=', 'vendors.id')
        ->get();
        $softwareAsset = SoftwareAsset::select('software_assets.id','software_assets.srno','softwares.software','software_categories.software_category','vendors.firstname','vendors.lastname','software_assets.price','software_assets.purchasedate','software_assets.warantee')
        ->join('softwares', 'software_assets.softwareId', '=', 'softwares.id')
        ->join('software_categories', 'software_assets.catagoryId', '=', 'software_categories.id')
        ->join('vendors', 'software_assets.vendorId', '=', 'vendors.id')
        ->get();
        $data = ['user' => $user, 'vendor' => $vendor, 'hardwareAsset' => $hardwareAsset, 'softwareAsset' => $softwareAsset];
        return response()->json($data);
    }
}
