<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HardwareRequest;
use App\Hardware;
use App\Vendor;
use App\HardwareAsset;
use Illuminate\Support\Facades\Input;

class HardwareController extends Controller
{
    public function index(Request $request)
    {
        $hardwareAsset = HardwareAsset::select('hardware_assets.id','hardwares.hardware','hardware_assets.model','hardware_assets.srno','vendors.firstname','vendors.lastname','hardware_assets.price','hardware_assets.purchasedate','hardware_assets.warantee')
        ->leftjoin('hardwares', 'hardware_assets.hardwarecategoryId', '=', 'hardwares.id')
        ->leftjoin('vendors', 'hardware_assets.vendorId', '=', 'vendors.id')
        ->get();
        return view('hardware.hardware', ['hardwareAsset' => $hardwareAsset]);
    }

    public function create(Request $request)
    {
        $hardware = Hardware::orderby('id', 'DESC')->get()->keyby('id');
        $vendor = Vendor::all()->keyBy('id');
        return view('hardware.addHardware', ['hardware' => $hardware, 'vendor' => $vendor]);
    }

    public function store(HardwareRequest $request)
    {
        $hardwareAsset = new HardwareAsset;
        $hardwareAsset->hardwarecategoryId = $request->hardwarecategoryId;
        $hardwareAsset->model = $request->model;
        $hardwareAsset->srno = $request->srno;
        $hardwareAsset->vendorId = $request->vendorId;
        $hardwareAsset->price = $request->price;
        $hardwareAsset->purchasedate = $request->purchasedate;
        $hardwareAsset->warantee = $request->warantee;
        $hardwareAsset->save();
     
        return redirect('hardware/create')->with('success', 'Hardware Add Successfully');
    }

    public function destroy(Request $request)
    {
        $hardwareAsset = HardwareAsset::where('id' ,'=', $request->deleteId)->first();
        $hardwareAsset->delete();
        return redirect('hardware');
    }

    public function getSearchHardwareAsset(Request $request)
    {
        $q = Input::get ( 'q' );
        $hardwareAsset = HardwareAsset::select('hardware_assets.id','hardwares.hardware','hardware_assets.model','hardware_assets.srno','vendors.firstname','vendors.lastname','hardware_assets.price','hardware_assets.purchasedate','hardware_assets.warantee')
                ->join('hardwares', 'hardware_assets.hardwarecategoryId', '=', 'hardwares.id')
                ->join('vendors', 'hardware_assets.vendorId', '=', 'vendors.id')
                ->where('srno','LIKE','%'.$q.'%')->orWhere('model','LIKE','%'.$q.'%')->orWhere('price','LIKE','%'.$q.'%')->get();
        if(count($hardwareAsset) > 0){
            return view('hardware.hardware', ['hardwareAsset' => $hardwareAsset]);
        }
        else {
            return view ('hardware.hardware', ['hardwareAsset' => $hardwareAsset]);
        }
    }
    
}
