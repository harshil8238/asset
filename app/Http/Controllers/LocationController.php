<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;

class LocationController extends Controller
{
    public function createCountry(Request $request)
    {
        return view('location.country');
    }
    public function setCountry(Request $request)
    {
        $validatedData = $request->validate([
            'country' => 'required',
        ]);
        $country = new Country;
        $country->name = $request->country;
        $country->save();

        return redirect('location/country')->with('success', 'Country Add Successfully');
    }
    public function displayCountry(Request $request)
    {
        $country = Country::all();
        return view('location.displayCountry', ['country'=>$country]);
    }
    public function deleteCountry(Request $request)
    {
        dd($_POST['deletedId']);
        $country = Country::all();
        return view('location.displayCountry', ['country'=>$country]);
    }
    
    public function createState(Request $request)
    {
        $country = Country::all();
        return view('location.state', ['country'=>$country]);
    }
    public function setState(Request $request)
    {
        $validatedData = $request->validate([
            'countryid' => 'required',
            'state' => 'required',
        ]);
        $state = new State;
        $state->country_id = $request->countryid;
        $state->state = $request->state;
        $state->save();

        return redirect('location/state')->with('success', 'State Add Successfully');
    }

    public function displayState(Request $request)
    {
        $state = State::select('states.id', 'states.state', 'countries.name')
        ->join('countries', 'states.country_id', '=', 'countries.id')
        ->get();
        return view('location.displayState', ['state'=>$state]);
    }

    public function createCity(Request $request)
    {
        $state = State::all();
        return view('location.city', ['state'=>$state]);
    }
    public function setCity(Request $request)
    {
        $validatedData = $request->validate([
            'stateid' => 'required',
            'city' => 'required',
        ]);
        $city = new City;
        $city->stateid = $request->stateid;
        $city->city = $request->city;
        $city->save();

        return redirect('location/city')->with('success', 'City Add Successfully');
    }
    public function displayCity(Request $request)
    {
        $city = City::select('cities.id', 'cities.city', 'states.state')
        ->join('states', 'cities.stateid', '=', 'states.id')
        ->get();
        return view('location.displayCity', ['city'=>$city]);
    }
    
}
