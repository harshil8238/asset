<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Country;
use App\User;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = User::select('users.id','users.firstname','users.lastname','users.email','users.phonenumber','users.address','cities.city','states.state','countries.name','users.pincode')
                        ->join('cities', 'users.cityId', '=', 'cities.id')
                        ->join('states', 'users.stateId', '=', 'states.id')
                        ->join('countries', 'users.countryId', '=', 'countries.id')
                        ->get();
        return view('user.user', ['user' => $user]);

    }

    public function create(Request $request)
    {
        $country = Country::all();
        return view('user.addUser', ['country'=>$country]);
    }

    public function store(UserRequest $request)
    {
        $user = new User;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phonenumber = $request->phonenumber;
        $user->address = $request->address;
        $user->countryId = $request->countryId;
        $user->stateId = $request->stateId;
        $user->cityId = $request->cityId;
        $user->pincode = $request->pincode;
        $user->save();
        return redirect('user/create')->with('success', 'User Add Successfully');
  
    }

    public function destroy(Request $request)
    {
        $user = User::where('id' ,'=', $request->deleteId)->first();
        $user->delete();
        return redirect('user');
    }

    public function getSearchUser(Request $request)
    {
    $q = Input::get ( 'q' );
    $user = User::select('users.id','users.firstname','users.lastname','users.email','users.phonenumber','users.address','cities.city','states.state','countries.name','users.pincode')
                ->join('cities', 'users.cityId', '=', 'cities.id')
                ->join('states', 'users.stateId', '=', 'states.id')
                ->join('countries', 'users.countryId', '=', 'countries.id')
                ->where('firstname','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->get();
    if(count($user) > 0){
        return view('user.user', ['user' => $user]);
    }
    else 
        return view ('user.user', ['user' => $user]);
    }
    
}
