<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\VendorRequest;
use App\Country;
use App\State;
use App\City;
use App\Vendor;
use Illuminate\Support\Facades\Input;

class VendorController extends Controller
{
    public function index(Request $request)
    {   
        $vendor = Vendor::select('vendors.id', 'vendors.firstname','vendors.lastname','vendors.shopname','vendors.email','vendors.phonenumber','vendors.address','cities.city','states.state','countries.name','vendors.pincode')
                        ->join('cities', 'vendors.cityId', '=', 'cities.id')
                        ->join('states', 'vendors.stateId', '=', 'states.id')
                        ->join('countries', 'vendors.countryId', '=', 'countries.id')
                        ->get();
        return view('vendor.vendor', ['vendor' => $vendor]);
    }
    
    public function create(Request $request)
    {
        $country = Country::all();
        return view('vendor.addVendor', ['country'=>$country]);
    }

    public function store(VendorRequest $request)
    {
        $vendor = new Vendor;
        $vendor->firstname = $request->firstname;
        $vendor->lastname = $request->lastname;
        $vendor->shopname = $request->shopname;
        $vendor->email = $request->email;
        $vendor->phonenumber = $request->phonenumber;
        $vendor->address = $request->address;
        $vendor->countryId = $request->countryId;
        $vendor->stateId = $request->stateId;
        $vendor->cityId = $request->cityId;
        $vendor->pincode = $request->pincode;
        $vendor->save();
     
        return redirect('vendor/create')->with('success', 'Vendor Add Successfully');
    }
    
    public function destroy(Request $request)
    {
        $vendor = Vendor::where('id' ,'=', $request->deleteId)->first();
        $vendor->delete();
        return redirect('vendor')->with('success', 'Vendor Delete Successfully');;
    }

    public function getState(Request $request)
    {
        $state = State::where('country_id', '=', $request->countryID)->get();
        return response()->json($state);
    }
    public function getCity(Request $request)
    {
        $city = City::where('stateid', '=', $request->stateId)->get();
        return response()->json($city);
    }
    public function getSearchVendor(Request $request)
    {
        $q = Input::get ( 'q' );
        $vendor = Vendor::select('vendors.firstname','vendors.lastname','vendors.shopname','vendors.email','vendors.phonenumber','vendors.address','cities.city','states.state','countries.name','vendors.pincode')
                ->join('cities', 'vendors.cityId', '=', 'cities.id')
                ->join('states', 'vendors.stateId', '=', 'states.id')
                ->join('countries', 'vendors.countryId', '=', 'countries.id')
                ->where('firstname','LIKE','%'.$q.'%')->orWhere('lastname','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->get();
        if(count($vendor) > 0){
            return view('vendor.vendor', ['vendor' => $vendor]);
        }
        else {
            return view ('vendor.vendor', ['vendor' => $vendor]);
        }
    }
    public function deleteVendor(Request $request)
    {
        dd($request);
    }
    
    
}
