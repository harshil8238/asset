<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    public function postLogin(Request $request)
    {   
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        $userdata = $request->only(['email', 'password']);
        if(Auth::attempt($userdata)){
            return redirect('dashboard');
        }

        return back()->with('error', 'id & pswd not match');
    }

    public function logout(Request $request)
    {   
        Auth::logout();
        return redirect('/');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
