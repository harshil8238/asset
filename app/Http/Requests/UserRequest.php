<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|alpha',
            'lastname' => 'required|alpha',
            'email' => 'required|email',
            'password' => 'required',
            'phonenumber' => 'required|numeric|digits:10',
            'address' => 'required|max:355',
            'countryId' => 'required|numeric',
            'stateId' => 'required|numeric',
            'cityId' => 'required|numeric',
            'pincode' => 'required|numeric',
        ];
    }
}
