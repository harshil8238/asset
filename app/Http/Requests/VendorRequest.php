<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shopname' => 'required',
            'firstname' => 'required|alpha',
            'lastname' => 'required|alpha',
            'email' => 'required|email',
            'phonenumber' => 'required|numeric|digits:10',
            'address' => 'required|max:355',
            'countryId' => 'required|numeric',
            'stateId' => 'required|numeric',
            'cityId' => 'required|numeric',
            'pincode' => 'required|numeric',
        ];
    }
}
