
$("#addVendor").click(function() {
    $('#vendorForm').parsley();
});
$("#addUser").click(function() {
    $('#userForm').parsley();
});
$("#addSoftwareCategory").click(function() {
    $('#softwareCategoryForm').parsley();
});
$("#addSoftware").click(function() {
    $('#createSoftwareForm').parsley();
});

$("#addHardware").click(function() {
    $('#createHardwareForm').parsley();
});

$("#addHardwareAsset").click(function() {
    $('#hardwareAssetForm').parsley();
});

$("#addSoftwareAsset").click(function() {
    $('#softwareAssetForm').parsley();
});

$("#addCountry").click(function() {
    $('#countryForm').parsley();
});

$("#addState").click(function() {
    $('#stateForm').parsley();
});

$("#addCity").click(function() {
    $('#cityForm').parsley();
});



