$(document).ready(function(){

    $("#catagoryId").on('change', function(e) {
        var categoryID = $(this).val();
        console.log(categoryID);
 
        if (categoryID) 
        {            
            jQuery.ajax({
                url:'/software/getSoftwareName',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    categoryID: categoryID,
                },
                success: function( data ){
                    $("#softwareId").prop("disabled", false);  
                    $.each(data, function( index, value ) {
                        $( "#softwareId" ).append( "<option value="+value.id+">"+value.software+"</option>" );
                    });
                },
                error: function (xhr, b, c) {
                    console.log("error");
                }
            });
        }
        
    })

    var subcategory = $("#getsubcategoty").val();
    if (subcategory) 
        {        
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url:'/software/getsubCategory',
                type: 'POST',
                data: {
                    subCategoryID: subcategory,
                },
                success: function( data ){
                    $("#getsubcategoty").html(data.software.toUpperCase());
                },
                error: function (xhr, b, c) {
                    console.log("error");
                }
            });
        }
    
});