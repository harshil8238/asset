$(document).ready(function(){

    $("#countryId").on('change', function(e) {

        var countryId = $(this).val();
        
        jQuery.ajax({
            url:'/vendor/getState',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                countryID: countryId,
            },
            success: function( data ){
                $("#stateId").prop("disabled", false);  
                $.each(data, function( index, value ) {
                    $( "#stateId" ).append( "<option value="+value.id+">"+value.state+"</option>" );
                });
            },
            error: function (xhr, b, c) {
            }
        });    
    });

    $("#stateId").on('change', function(e) {

        var stateId = $(this).val();
    
        jQuery.ajax({
            url:'/vendor/getCity',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                stateId: stateId,
            },
            success: function( data ){
                console.log(data);
                $("#cityId").prop("disabled", false);  
                $.each(data, function( index, value ) {
                    $( "#cityId" ).append( "<option value="+value.id+">"+value.city+"</option>" );
                });
            },
            error: function (xhr, b, c) {
            }
        });    
    });
    
    
});