$(document).ready(function(){

    if ("http://localhost:8080/Harshil/asset/public/dashboard" == window.location.href) {
        
        jQuery.ajax({
            url:'http://localhost:8080/Harshil/asset/public/dashboard/getinfo',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( data ){
                
                $.each(data.softwareAsset, function( index, value ) {             
                    var d = new Date(value.purchasedate);
                    var todayDate = new Date();
                    var warantee = value.warantee;
                    var year = d.getFullYear();
                    var month = d.getMonth() ;
                    var day = d.getDate();
                    var beforeExpirydate = new Date(year + warantee, month, day-7);
                    var expirydate = new Date(year + warantee, month, day);
              
                    if (todayDate >= beforeExpirydate && todayDate < expirydate) {
                       $("#notifyExpiry").append( "<tr> <td>"+value.srno+"</td><td>"+value.software+"</td> <td>"+value.purchasedate+"</td> <td>"+expirydate+"</td> </tr>" );
                    }
                });
                $.each(data.hardwareAsset, function( index, value ) {             
                    var d = new Date(value.purchasedate);
                    var todayDate = new Date();
                    var warantee = value.warantee;
                    var year = d.getFullYear();
                    var month = d.getMonth() ;
                    var day = d.getDate();
                    var beforeExpirydate = new Date(year + warantee, month, day-7);
                    var expirydate = new Date(year + warantee, month, day);
                   
                    if (todayDate >= beforeExpirydate && todayDate < expirydate) {
                       $("#notifyExpiry").append( "<tr> <td>"+value.srno+"</td><td>"+value.hardware+"</td> <td>"+value.purchasedate+"</td> <td>"+expirydate+"</td> </tr>" );
                    }
                });
                
                $("#hardwareAssetCount").html(data.hardwareAsset.length);
                $("#softwareAssetCount").html(data.softwareAsset.length);
                $("#userCount").html(data.user.length);
                $("#vendorCount").html(data.vendor.length);
            },
            error: function (xhr, b, c) {
            }
        });   
    }

});