<?php

Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('login');
})->name('login');
Route::post('/login', 'Auth\LoginController@postLogin');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    });
    Route::resource('/vendor', 'VendorController');
    Route::post('/searchUser', 'UserController@getSearchUser');
    Route::post('/searchVendor', 'VendorController@getSearchVendor');
    Route::post('/searchHardwareAsset', 'HardwareController@getSearchHardwareAsset');
    Route::post('/searchSoftwareAsset', 'SoftwareController@getSearchSoftwareAsset');
    Route::resource('/user', 'UserController');
    Route::resource('/hardware', 'HardwareController');
    Route::resource('/software', 'SoftwareController');
    Route::post('/software/getSoftwareName', 'SoftwareController@getSoftwareName');
    Route::post('/dashboard/getinfo', 'DashboardController@getInfo');
    Route::group(['prefix' => 'asset'], function () {
        Route::get('/createSoftwareCategory', 'AssetController@createSoftwareCategory');
        Route::get('/softwareCategory', 'AssetController@softwareCategory'); 
        Route::post('/softwareCategory', 'AssetController@storeSoftwareCategory');   
        Route::post('/destroySoftwareCategory', 'AssetController@destroySoftwareCategory');        
        Route::get('/createSoftware', 'AssetController@createSoftware');
        Route::get('/software', 'AssetController@software');
        Route::post('/software', 'AssetController@storeSoftware');
        Route::get('/createHardware', 'AssetController@createHardware');
        Route::get('/hardware', 'AssetController@hardware');
        Route::post('/hardware', 'AssetController@storeHardware');
    });
    Route::post('/vendor/getState', 'VendorController@getState'); 
    Route::post('/vendor/getCity', 'VendorController@getCity');
    Route::post('/vendorDelete', 'VendorController@deleteVendor'); 
    Route::group(['prefix' => 'location'], function () {
        Route::get('/country', 'LocationController@createCountry');
        Route::get('/displayCountry', 'LocationController@displayCountry');
        Route::post('/createCountry', 'LocationController@setCountry');
        Route::post('/deleteCountry', ['as' => 'deleteCountry', 'uses' => 'LocationController@deleteCountry']);
        Route::get('/state', 'LocationController@createState');
        Route::get('/displayState', 'LocationController@displayState');
        Route::post('/createState', 'LocationController@setState');
        Route::get('/city', 'LocationController@createCity');
        Route::get('/displayCity', 'LocationController@displayCity');
        Route::post('/createCity', 'LocationController@setCity');
    });
    Route::get('/logout', 'Auth\LoginController@logout');
});
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
