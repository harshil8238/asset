@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create Software 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="createSoftwareForm" method="post" action="{{ asset('/asset/software') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                    
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="software_category">Software Category</label>
                            <select name="software_category_id" class="form-control" required id="software_category_id">
                                <option value="{{ old('software_category_id') }}"> @if( old('software_category_id') ) {{ $softwareCategory[old('software_category_id')]->software_category }} @else SELECT @endif </option>
                                @foreach($softwareCategory as $value)
                                    <option value="{{ $value->id }}">{{ $value->software_category  }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('software_category_id'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('software_category_id') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group">
                            <label for="software">Software</label>
                            <input type="text" required class="form-control" name="software" id="software"  placeholder=" Software" value="{{ old('software') }}">
                        </div>
                        @if($errors->first('software'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('software') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group">
                            <label for="photo"> File</label>
                            <input type="file" required class="form-control" id="photo" name="photo" value="{{ old('photo') }}">
                        </div>     
                        @if($errors->first('photo'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('photo') }} </strong>  
                            </div> 
                        @endif 

                    </div>
    
                    <div class="box-footer">
                        <button type="submit" id="addSoftware" class="btn btn-primary">Create Software </button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
