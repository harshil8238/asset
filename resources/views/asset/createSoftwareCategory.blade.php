@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create Software Category
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" method="post" id="softwareCategoryForm" action="{{ asset('/asset/softwareCategory') }}">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="software_category">Software Category</label>
                            <input type="text" required class="form-control" name="software_category" id="software_category" placeholder="Enter Software Category">
                        </div>
                        @if($errors->first('software_category'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('software_category') }} </strong>  
                            </div> 
                        @endif
                    </div>
                   
                    <div class="box-footer">
                        <button type="submit" id="addSoftwareCategory" class="btn btn-primary">Add Software Category</button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
