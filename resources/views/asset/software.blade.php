@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Software 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Software Name</th>
                        <th>Software Category</th>
                        <th>Photo</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($software as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ strtoupper($value->software) }}</td>
                        <td>{{ strtoupper($value->software_category->software_category) }}</td>
                        <td><img src="{{ asset( 'storage/' . $value->photo) }}" height="50" width="50"></td>
                        <td> 
                            <a class="btn btn-primary">Edit</a>
                            <a class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
