@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Software Category
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Software Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($softwareCategory as $key => $value)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $value->software_category }}</td>
                                <td> 
                                    <a class="btn btn-primary">Edit</a>
                                    <form action="{{ asset('/asset/destroySoftwareCategory') }}" method="post">
                                        @csrf
                                        <input class="btn btn-danger" onclick="return confirm('Are you sure?')" type="submit" value="Delete" />
                                        <input type="hidden" name="deleteId" value="{{ $value->id }}">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
