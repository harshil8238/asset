@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Hardware 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Hardware Name</th>
                            <th>Photo</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($hardware as $key => $value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ strtoupper($value->hardware) }}</td>
                            <td><img src="{{ asset('storage/'.$value->photo) }}" height="50" width="50"></td>
                            <td> 
                                <a class="btn btn-primary">Edit</a>
                                <a class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
