@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create Hardware 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="createHardwareForm" method="post" action="{{ asset('/asset/hardware') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="hardware">Hardware</label>
                                <input type="text" required class="form-control" name="hardware" id="hardware" placeholder="Enter Hardware" value="{{ old('hardware') }}">
                            </div>
                        </div>
                        @if($errors->first('hardware'))
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <div class="alert alert-danger ">  
                                        <strong> {{ $errors->first('hardware') }} </strong>  
                                    </div> 
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="photo"> File</label>
                                <input type="file" required class="form-control" id="photo" name="photo" value="{{ old('photo') }}">
                            </div>    
                        </div>  
                        @if($errors->first('photo'))
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <div class="alert alert-danger ">  
                                        <strong> {{ $errors->first('photo') }} </strong>  
                                    </div> 
                                </div>
                            </div>
                        @endif
                    </div>
                
                    <div class="box-footer">
                        <button type="submit" id="addHardware" class="btn btn-primary">Create Hardware </button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
