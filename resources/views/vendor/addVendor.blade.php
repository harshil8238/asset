@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Add Vendor
        </h1>      
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="vendorForm" method="post" action="{{ asset('/vendor') }}">
                    @csrf
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="box-body">
                    @if (session('success'))
                        <div class="form-group col-md-12 alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="row">

                        <div class="form-group col-lg-6">
                            <label for="firstname">FirstName</label>
                            <input type="text" class="form-control " required id="firstname" placeholder="Enter Firstname" name="firstname" value="{{ old('firstname')}}">
                        </div>
                        @if($errors->first('firstname'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('firstname') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="lastname">LastName</label>
                            <input type="text" class="form-control" required id="lastname" placeholder="Enter Lastname" name="lastname" value="{{ old('lastname')}}">
                        </div>
                        @if($errors->first('lastname'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('lastname') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6 ">
                            <label for="shopname">Shop Name</label>
                            <input type="text" class="form-control" required id="shopname" name="shopname" placeholder="Enter shopname" value="{{ old('shopname')}}" >
                        </div>
                        @if($errors->first('shopname'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('shopname') }} </strong>  
                            </div> 
                        @endif
                        
                        <div class="form-group col-lg-6">
                            <label for="email">E-Mail</label>
                            <input type="email" class="form-control" required id="email" placeholder="Enter E-Mail" name="email" value="{{ old('email')}}">
                        </div>
                        @if($errors->first('email'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('email') }} </strong>  
                            </div> 
                        @endif
                        
                        <div class="form-group col-lg-6">
                            <label for="phonenumber">Phone Number</label>
                            <input type="text" class="form-control" required id="phonenumber" placeholder="Enter Phonenumber" name="phonenumber" value="{{ old('phonenumber')}}">
                        </div>
                        @if($errors->first('phonenumber'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('phonenumber') }} </strong>  
                            </div> 
                        @endif
                        
                        <div class="form-group col-lg-6">
                            <label for="address">Address Firstline</label>
                            <input type="text" class="form-control" required id="address" placeholder="Enter Address" name="address" value="{{ old('address')}}">
                        </div>
                        @if($errors->first('address'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('address') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="countryId">Country</label>
                            <select name="countryId" class="form-control" required id="countryId">
                                <option value="">SELECT</option>
                                @foreach($country as $value) {
                                    <option value="{{$value->id}}" @if($value->id == old('countryId')) selected @endif>{{ strtoupper($value->name) }} </option>
                                }
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('countryId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('countryId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="stateId">State</label>
                            <select name="stateId" class="form-control" required id="stateId" disabled>
                                <option value="">SELECT</option>
                            </select>
                        </div>
                        @if($errors->first('stateId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('stateId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="cityId">City</label>
                            <select name="cityId" class="form-control" required id="cityId" disabled>
                                <option value="">SELECT</option>
                            </select>
                        </div>
                        @if($errors->first('cityId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('cityId') }} </strong>  
                            </div> 
                        @endif
                        
                        <div class="form-group col-lg-6">
                            <label for="pincode">Pincode</label>
                            <input type="text" class="form-control" required id="pincode" placeholder="Enter Pincode" name="pincode" value="{{ old('pincode')}}">
                        </div>
                        @if($errors->first('pincode'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('pincode') }} </strong>  
                            </div> 
                        @endif

                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" id="addVendor" class="btn btn-primary">Add Vendor</button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection

