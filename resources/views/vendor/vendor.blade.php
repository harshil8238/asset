@extends('layouts.app')

@section('content')
<section class="content-header">
        <div class="row">
            <h3 class="col-lg-4">
                Vendor
            </h3>  
            <form class="col-lg-offset-4 col-lg-4" action="{{ asset('/searchVendor') }}" method="POST" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q"
                        placeholder="Search users"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </section>

    <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    @if (session('success'))
                        <div class="form-group col-md-12 alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th>Shop Name</th>
                                <th>E-mail</th>
                                <th>Phone Number</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Pincode</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($vendor as $value) {   
                            <tr>
                                <td>{{ $value->firstname }}</td>
                                <td>{{ $value->lastname }}</td>
                                <td>{{ $value->shopname }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->phonenumber }}</td>
                                <td>{{ $value->address }}</td>
                                <td>{{ $value->city }}</td>
                                <td>{{ $value->state }}</td>
                                <td>{{ $value->name }} </td>
                                <td>{{ $value->pincode }}</td>
                                <td> 
                                <a class="btn btn-primary" <?php  $user = Auth::user(); if($user->firstname != "admin"){ ?> disabled <?php } ?>  >Edit</a>
                                <form action="{{ asset('/vendor/vendor') }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input class="btn btn-danger" onclick="return confirm('Are you sure?')" <?php if($user->firstname != "admin"){ ?> disabled <?php } ?> type="submit" value="Delete" />
                                    <input type="hidden" name="deleteId" value="{{ $value->id }}">
                                </form>
                                </td>
                            </tr>
                        }
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
