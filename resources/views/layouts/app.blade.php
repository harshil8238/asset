<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" href="{!! asset('asset.png') !!}"/>
  <link rel="stylesheet" href="{{ asset('theme/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('theme/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>AMS</b></span>
      <span class="logo-lg"><b>Asset Management </b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
                            
        <ul class="nav navbar-nav">
          <li class="user-footer">
            <div class=" pull-right">
                <a href="{{ asset('logout') }}" style="padding-bottom: 15px;" class="btn btn-primary btn-lg btn-flat">Log out</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->
<?php  $user = Auth::user();  ?>
@if ($user->firstname == "admin")
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('theme/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ $user->firstname }}</p>
         <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>
     
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/dashboard') }}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Vendor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/vendor/create') }}"><i class="fa fa-circle-o"></i> Add Vendor</a></li>
            <li><a href="{{ asset('/vendor') }}"><i class="fa fa-circle-o"></i> Display Vendor</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/user/create') }}"><i class="fa fa-circle-o"></i> Add User</a></li>
            <li><a href="{{ asset('/user') }}"><i class="fa fa-circle-o"></i> Display User</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-wrench"></i> <span>Asset</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/asset/createSoftwareCategory') }}"><i class="fa fa-circle-o"></i> Create Software Category</a></li>
            <li><a href="{{ asset('/asset/softwareCategory') }}"><i class="fa fa-circle-o"></i> Display Software Category</a></li>
            <li><a href="{{ asset('/asset/createSoftware') }}"><i class="fa fa-circle-o"></i> Create Software</a></li>
            <li><a href="{{ asset('/asset/software') }}"><i class="fa fa-circle-o"></i> Display Software</a></li>    
            <li><a href="{{ asset('/asset/createHardware') }}"><i class="fa fa-circle-o"></i> Create Hardware</a></li>
            <li><a href="{{ asset('/asset/hardware') }}"><i class="fa fa-circle-o"></i> Display Hardware</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Hardware Asset</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/hardware/create') }}"><i class="fa fa-circle-o"></i> Add Hardware</a></li>
            <li><a href="{{ asset('/hardware') }}"><i class="fa fa-circle-o"></i> Display Hardware</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-spinner"></i> <span>Software</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/software/create') }}"><i class="fa fa-circle-o"></i> Add Software</a></li>
            <li><a href="{{ asset('/software') }}"><i class="fa fa-circle-o"></i> Display Software</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Location</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/location/country') }}"><i class="fa fa-circle-o"></i> Add Country</a></li>
            <li><a href="{{ asset('/location/displayCountry') }}"><i class="fa fa-circle-o"></i> Display Country</a></li>
            <li><a href="{{ asset('/location/state') }}"><i class="fa fa-circle-o"></i> Add State</a></li>
            <li><a href="{{ asset('/location/displayState') }}"><i class="fa fa-circle-o"></i> Display State</a></li>
            <li><a href="{{ asset('/location/city') }}"><i class="fa fa-circle-o"></i> Add City</a></li>
            <li><a href="{{ asset('/location/displayCity') }}"><i class="fa fa-circle-o"></i> Display City</a></li>
          </ul>
        </li>

      </ul>
    </section>
  </aside>
@else
<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('theme/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ $user->firstname }} {{ $user->lastname }}</p>
         <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>
     
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/dashboard') }}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>


    
        <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Hardware Asset</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/hardware/create') }}"><i class="fa fa-circle-o"></i> Add Hardware</a></li>
            <li><a href="{{ asset('/hardware') }}"><i class="fa fa-circle-o"></i> Display Hardware</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-spinner"></i> <span>Software</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/software/create') }}"><i class="fa fa-circle-o"></i> Add Software</a></li>
            <li><a href="{{ asset('/software') }}"><i class="fa fa-circle-o"></i> Display Software</a></li>
          </ul>
        </li>

      </ul>
    </section>
  </aside>
@endif
  <!-- =============================================== -->

  <div class="content-wrapper">
        @yield('content')
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2018-19 Dharmesh & Harshil </strong> All rights
    reserved.
  </footer>

<script src="{{ asset('theme/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('theme/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('theme/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('theme/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('theme/dist/js/demo.js') }}"></script>
<script src="{{ asset('theme/dist/js/location/location.js') }}"></script>
<script src="{{ asset('theme/dist/js/softwareAsset/addSoftwareAsset.js') }}"></script>
<script src="{{ asset('theme/build/js/parsley.js') }}"></script>
<script src="{{ asset('theme/dist/js/customValidation/vendorValidation.js') }}"></script>
<script src="{{ asset('theme/dist/js/dashboard/dashboard.js') }}"></script>



<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
