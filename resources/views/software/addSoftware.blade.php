@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Add Software Asset
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="softwareAssetForm" method="post" action="{{ asset('/software') }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group col-lg-6">
                            <label for="catagoryId">Catagory-Id</label>
                            <select name="catagoryId" class="form-control" required id="catagoryId">
                                <option value="{{ old('catagoryId')}}">@if(old('catagoryId')) {{ strtoupper($softwareCategory[old('catagoryId')]->software_category) }} @else SELECT @endif </option>
                                @foreach($softwareCategory as $value)
                                    <option value="{{ $value->id }}">{{ strtoupper($value->software_category) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('catagoryId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('catagoryId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="softwareId">Software-Name</label>
                            <select name="softwareId" class="form-control" required id="softwareId" disabled>
                                <option value="{{ old('softwareId')}}">@if(old('softwareId')) {{ old('softwareId')}} @else SELECT @endif </option>
                            </select>
                        </div>
                        @if($errors->first('softwareId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('softwareId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="productkey">Product-Key</label>
                            <input type="text" class="form-control" required id="productkey" placeholder="Enter Product-Key" name="productkey" value="{{ old('productkey') }}">
                        </div>
                        @if($errors->first('productkey'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('productkey') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="srno">SR-NO</label>
                            <input type="text" class="form-control" required id="srno" placeholder="Enter SR-No" name="srno" value="{{ old('srno') }}">
                        </div>
                        @if($errors->first('srno'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('srno') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="vendorId">Vendor</label>
                            <select name="vendorId" class="form-control" required id="vendorId" >
                            <option value="{{ old('vendorId')}}">@if(old('vendorId')) {{ strtoupper($vendor[old('vendorId')]->firstname) }} {{ strtoupper($vendor[old('vendorId')]->lastname) }} @else SELECT @endif </option>
                                @foreach($vendor as $value)
                                <option value="{{ $value->id }}">{{ strtoupper($value->firstname) }} {{ strtoupper($value->lastname) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('vendor'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('vendor') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" required id="price" placeholder="Enter Price" name="price" value="{{ old('price') }}">
                        </div>
                        @if($errors->first('price'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('price') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="purchasedate">Purchase-Date</label>
                            <input type="date" class="form-control" required id="purchasedate" placeholder="Enter Date" name="purchasedate" value="{{ old('purchasedate') }}">
                        </div>
                        @if($errors->first('purchasedate'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('purchasedate') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="warantee">Warantee</label>
                            <input type="text" class="form-control" required id="warantee" placeholder="Enter Warantee Year" name="warantee" value="{{ old('warantee') }}">
                        </div>
                        @if($errors->first('warantee'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('warantee') }} </strong>  
                            </div> 
                        @endif
                                        
                    </div>

                    <div class="box-footer">
                        <button type="submit" id="addSoftwareAsset" class="btn btn-primary">Add Software-Asset</button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
