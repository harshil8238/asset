@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Country
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Country</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($country as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td> 
                                    <a href="" class="btn btn-primary">Edit</a>
                                    <form action="deleteCountry" method="post">
                                        <input class="btn btn-danger" type="submit" value="Delete" />
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="deleteId" value="{{ $value->id }}" />
                                        {!! csrf_field() !!}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
