@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create Country 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="countryForm" method="post" action="{{ asset('/location/createCountry') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-6 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="country">Country</label>
                                <input type="text" class="form-control" name="country" required id="country" placeholder="Enter Country" value="{{ old('country') }}">
                            </div>
                        </div>
                        @if($errors->first('country'))
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <div class="alert alert-danger ">  
                                        <strong> {{ $errors->first('country') }} </strong>  
                                    </div> 
                                </div>
                            </div>
                        @endif
                    </div>
                
                    <div class="box-footer">
                        <button type="submit" id="addCountry" class="btn btn-primary">Create Country </button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
