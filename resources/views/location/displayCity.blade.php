@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            City
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($city as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->city }}</td>
                                <td>{{ $value->state }}</td>
                                <td> 
                                    <a class="btn btn-primary">Edit</a>
                                    <a class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
