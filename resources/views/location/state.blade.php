@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create State 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="stateForm" method="post" action="{{ asset('/location/createState') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                   
                        <div class="form-group">
                            <label for="countryid">Country</label>
                            <select name="countryid" required class="form-control" id="countryid">
                                <option value="">SELECT</option>
                                @foreach($country as $value) {
                                    <option value="{{$value->id}}" @if($value->id == old('countryid')) selected @endif>{{ strtoupper($value->name) }} </option>
                                }
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('countryid'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('countryid') }} </strong>  
                            </div> 
                        @endif

                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="state">State</label>
                                <input type="text" required class="form-control" name="state" id="state" placeholder="Enter State" value="{{ old('state') }}">
                            </div>
                        </div>
                        @if($errors->first('state'))
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <div class="alert alert-danger ">  
                                        <strong> {{ $errors->first('state') }} </strong>  
                                    </div> 
                                </div>
                            </div>
                        @endif
                    </div>
                
                    <div class="box-footer">
                        <button type="submit" id="addState" class="btn btn-primary">Create State </button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
