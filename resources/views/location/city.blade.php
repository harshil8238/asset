@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Create City 
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <form role="form" id="cityForm" method="post" action="{{ asset('/location/createCity') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="state">State</label>
                            <select name="stateid" required class="form-control" id="stateid">
                                <option value="">SELECT</option>
                                @foreach($state as $value) {
                                    <option value="{{$value->id}}" @if($value->id == old('stateid')) selected @endif>{{ strtoupper($value->state) }} </option>
                                }
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('stateid'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('stateid') }} </strong>  
                            </div> 
                        @endif

                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="city">City</label>
                                <input required type="text" class="form-control" name="city" id="city" placeholder="Enter City" value="{{ old('city') }}">
                            </div>
                        </div>
                        @if($errors->first('city'))
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <div class="alert alert-danger ">  
                                        <strong> {{ $errors->first('city') }} </strong>  
                                    </div> 
                                </div>
                            </div>
                        @endif
                    </div>
                
                    <div class="box-footer">
                        <button type="submit" id="addCity" class="btn btn-primary">Create City </button>
                    </div>
                </form>
            </div>
            
        </div>
    </section>
@endsection
