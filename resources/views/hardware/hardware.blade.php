@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="row">
            <h3 class="col-lg-4">
                Hardware Asset
            </h3>  
            <form class="col-lg-offset-4 col-lg-4" action="{{ asset('/searchHardwareAsset') }}" method="POST" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q"
                        placeholder="Search users"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>   
    </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                        <th>SR NO</th>
                        <th>Asset Category</th>
                        <th>Model</th>
                        <th>Vendor</th>
                        <th>Price</th>
                        <th>Purchase-date</th>
                        <th>warantee status</th>   
                        <th>Action</th>
                        </tr>
                        </thead>
                        @foreach($hardwareAsset as $value) {   
                            <tbody><tr>
                                <td>{{ $value->srno }}</td>
                                <td>{{ $value->hardware }}</td>
                                <td>{{ $value->model }}</td>
                                <td>{{ $value->firstname }} {{ $value->lastname }}</td>
                                <td>{{ $value->price }}</td>
                                <td>{{ $value->purchasedate }}</td>
                                <td>{{ $value->warantee }}</td>
                                <td> 
                                    <a class="btn btn-primary"  <?php  $user = Auth::user(); if($user->firstname != "admin"){ ?> disabled <?php } ?> >Edit</a>
                                    <form action="{{ asset('/hardware/hardware') }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input class="btn btn-danger" onclick="return confirm('Are you sure?')" <?php if($user->firstname != "admin"){ ?> disabled <?php } ?> type="submit" value="Delete" />
                                        <input type="hidden" name="deleteId" value="{{ $value->id }}">
                                    </form>
                                </td>
                            </tr>
                        }
                        </tbody>
                        @endforeach
                       
                    </table>
                </div>
            </div>
            
        </div>
    </section>
@endsection
