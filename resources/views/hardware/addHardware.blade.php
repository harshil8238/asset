@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Add Hardware Asset
        </h1>      
        </section>

        <section class="content">

        <div class="box">
            <div class="box-header with-border">
               <form role="form" id="hardwareAssetForm" method="post" action="{{ asset('/hardware') }}">
               @csrf
                    <div class="box-body">
                        @if (session('success'))
                            <div class="form-group col-md-12 alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="form-group col-lg-6">
                            <label for="hardwarecategoryId">Asset-Name</label>
                            <select name="hardwarecategoryId" required class="form-control" id="hardwarecategoryId" value="{{ old('hardwarecategoryId')}}">
                                <option value="{{ old('hardwarecategoryId')}}">@if(old('hardwarecategoryId')) {{ strtoupper($hardware[old('hardwarecategoryId')]->hardware) }} @else SELECT @endif </option> 
                                @foreach($hardware as $value)
                                <option value="{{ $value->id }}">{{ strtoupper($value->hardware) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('hardwarecategoryId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('hardwarecategoryId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="model">Model-No</label>
                            <input type="text" class="form-control" required id="model" placeholder="Enter Model" name="model" value="{{ old('model')}}">
                        </div>
                        @if($errors->first('model'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('model') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="srno">SR-NO</label>
                            <input type="text" class="form-control" required id="srno" placeholder="Enter SR-No" name="srno" value="{{ old('srno')}}">
                        </div>
                        @if($errors->first('srno'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('srno') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="vendorId">vendorId</label>
                            <select name="vendorId" class="form-control" required id="vendorId" value="{{ old('vendorId')}}">
                                <option value="{{ old('vendorId')}}">@if(old('vendorId')) {{ strtoupper($vendor[old('vendorId')]->firstname) }} {{ strtoupper($vendor[old('vendorId')]->lastname) }} @else SELECT @endif </option>
                                @foreach($vendor as $value)
                                <option value="{{ $value->id }}">{{ strtoupper($value->firstname) }} {{ strtoupper($value->lastname) }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->first('vendorId'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('vendorId') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" required id="price" placeholder="Enter Price" name="price" value="{{ old('price')}}">
                        </div>
                        @if($errors->first('price'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('price') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="purchasedate">Purchase-Date</label>
                            <input type="date" class="form-control" required id="purchasedate" placeholder="Enter Date" name="purchasedate" value="{{ old('purchasedate')}}">
                        </div>
                        @if($errors->first('purchasedate'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('purchasedate') }} </strong>  
                            </div> 
                        @endif

                        <div class="form-group col-lg-6">
                            <label for="warantee">Warantee</label>
                            <input type="text" class="form-control" required id="warantee" placeholder="Enter Warantee Year" name="warantee" value="{{ old('warantee')}}">
                        </div>
                        @if($errors->first('warantee'))
                            <div class="alert alert-danger">  
                                <strong> {{ $errors->first('warantee') }} </strong>  
                            </div> 
                        @endif                
                    </div>

                    <div class="box-footer">
                    <button type="submit" id="addHardwareAsset" class="btn btn-primary">Add Hardware-Asset</button>
                    </div>
              </form>
            </div>
        </div>
    </section>
@endsection
